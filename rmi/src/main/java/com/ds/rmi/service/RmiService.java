package com.ds.rmi.service;

import com.ds.lab.model.dto.MedicationDTO;
import com.ds.lab.model.dto.MedicationPlanDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@EnableScheduling
public class RmiService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private List<MedicationPlanDTO> medPlans = new ArrayList<>();

    @Bean
    private RmiProxyFactoryBean service() {
        RmiProxyFactoryBean rmiProxyFactory = new RmiProxyFactoryBean();
        rmiProxyFactory.setServiceUrl("rmi://localhost:1099/MedicationService");
        rmiProxyFactory.setServiceInterface(MedicationService.class);
        rmiProxyFactory.afterPropertiesSet();
        return rmiProxyFactory;
    }

    public List<MedicationPlanDTO> getMedicationPlanning(String username) {
        MedicationService medicationService = (MedicationService) service().getObject();
        return medicationService.getMedicationPlansOfPatient(username);
    }

    public MedicationDTO takeMedication(long medId, String patient) {
        MedicationService medicationService = (MedicationService) service().getObject();
        return medicationService.takeMedication(medId, patient);
    }

    //in order to download medication plan each day at 6 AM: @Scheduled(cron = "0 0 6 * * *)

    @Scheduled(cron = "*/2 * * * * *")
    public void retrieveMedicationPlans() {
        MedicationService medicationService = (MedicationService) service().getObject();
        medPlans = medicationService.getMedicationPlansOfPatient("patient1");
        log.info("Retrieved Medication Plans");
    }
}

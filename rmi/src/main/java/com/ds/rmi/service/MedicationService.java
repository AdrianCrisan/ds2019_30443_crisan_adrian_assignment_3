package com.ds.rmi.service;

import com.ds.lab.model.dto.MedicationDTO;
import com.ds.lab.model.dto.MedicationPlanDTO;

import java.util.List;

public interface MedicationService {
    List<MedicationPlanDTO> getMedicationPlansOfPatient(String username);

    MedicationDTO takeMedication(long medId, String patient);
}

package com.ds.rmi.controller;

import com.ds.lab.model.dto.MedicationDTO;
import com.ds.lab.model.dto.MedicationPlanDTO;
import com.ds.rmi.service.RmiService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
public class MedicationController {

    private final RmiService rmiService;

    public MedicationController(RmiService rmiService) {
        this.rmiService = rmiService;
    }

    @GetMapping(path = "/api/medication")
    List<MedicationPlanDTO> getMedications(@RequestParam("user") String username) {
        return rmiService.getMedicationPlanning(username);
    }

    @PutMapping(path = "/api/medication")
    MedicationDTO takeMedication(@RequestParam("medId") long medId, @RequestParam("patient") String patient) {
        return rmiService.takeMedication(medId, patient);
    }


}

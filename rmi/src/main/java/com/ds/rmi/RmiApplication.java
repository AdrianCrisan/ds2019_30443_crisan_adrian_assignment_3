package com.ds.rmi;

import com.ds.rmi.service.MedicationService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;

@SpringBootApplication
public class RmiApplication {

    public static void main(String[] args) {
        SpringApplication.run(RmiApplication.class, args);
    }

}

package com.ds.lab.model.dto;

import java.io.Serializable;
import java.util.List;

public class MedicationPlanDTO implements Serializable {

    private long treatmentPeriod;
    private List<MedicationDTO> receivedMedications;

    public MedicationPlanDTO() {
    }

    public long getTreatmentPeriod() {
        return treatmentPeriod;
    }

    public void setTreatmentPeriod(long treatmentPeriod) {
        this.treatmentPeriod = treatmentPeriod;
    }

    public List<MedicationDTO> getReceivedMedications() {
        return receivedMedications;
    }

    public void setReceivedMedications(List<MedicationDTO> receivedMedications) {
        this.receivedMedications = receivedMedications;
    }

}
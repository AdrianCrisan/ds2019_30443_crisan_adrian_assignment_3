package com.ds.lab;

import com.ds.lab.service.ActivityService;
import com.ds.lab.service.ConsumerService;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

@SpringBootApplication
public class MedicalAssistanceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedicalAssistanceApplication.class, args);
	}

	private static final boolean NON_DURABLE = false;
	private static final String QUEUE_NAME = "activity";

	@Bean
	public Queue activity() {
		return new Queue(QUEUE_NAME, NON_DURABLE);
	}

	@Autowired
	private ActivityService activityService;

	@Autowired
	private ConsumerService consumerService;

	@RabbitListener(queues = QUEUE_NAME)
	public void listen(String in) {
		JsonObject jsonObject = new JsonParser().parse(in).getAsJsonObject();

		String activity = jsonObject.get("activity").toString().replaceAll("\"", "");
		String duration = jsonObject.get("duration").toString().replaceAll("\"", "");
		Long patientId = jsonObject.get("patientId").getAsLong();

		System.out.println("Message read from activity queue: " + in);

		String message = consumerService.checkDuration(activity, duration);
		if (patientId != null) {
			activityService.saveActivity(patientId, activity, duration);
		}


	}

}

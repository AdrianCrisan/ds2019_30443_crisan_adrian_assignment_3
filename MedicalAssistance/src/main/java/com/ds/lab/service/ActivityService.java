package com.ds.lab.service;

import com.ds.lab.model.Activities;
import com.ds.lab.model.Patient;
import com.ds.lab.repository.ActivitiesRepository;
import com.ds.lab.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityService {

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private ActivitiesRepository activitiesRepository;


    public void saveActivity(Long patientId, String activity, String duration) {

        Activities activityToSave = new Activities();
        activityToSave.setActivity(activity);
        activityToSave.setDuration(Long.parseLong(duration));

        Patient patient = patientRepository.findById(patientId).get();
        activityToSave.setPatient(patient);
        patient.addActivity(activityToSave);

        activitiesRepository.save(activityToSave);
        patientRepository.save(patient);
    }
}

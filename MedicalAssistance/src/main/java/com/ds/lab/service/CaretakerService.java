package com.ds.lab.service;

import com.ds.lab.model.Caretaker;
import com.ds.lab.model.Patient;
import com.ds.lab.model.dto.PatientDTO;
import com.ds.lab.repository.CaretakerRepository;
import com.ds.lab.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CaretakerService {

    @Autowired
    private CaretakerRepository caretakerRepository;

    @Autowired
    private PatientRepository patientRepository;

    public List<PatientDTO> getAllPatients(String username) {
        Caretaker caretaker = caretakerRepository.findByUsername(username);
        List<Patient> patients = patientRepository.findAllPatientsOfCaretaker(caretaker.getId());
        List<PatientDTO> patientDTOS = new ArrayList<>();

        for (Patient p: patients) {
            PatientDTO dto = new PatientDTO();
            dto.setId(p.getId());
            dto.setName(p.getName());
            dto.setAddress(p.getAddress());
            dto.setBirthDate(p.getBirthDate().toString());
            dto.setGender(p.getGender().toString());
            patientDTOS.add(dto);
        }
        return patientDTOS;
    }
}

package com.ds.lab.service;

import org.springframework.stereotype.Component;

@Component
public class ConsumerService {

    String message = "";

    public String checkDuration(String activity, String duration) {

        Long twelveHoursLong = 7 * 3600 * 1000L;
        Long oneHourLong = 200 * 1000L;

        System.out.println("1H = " + oneHourLong + "12H= " + twelveHoursLong );

        Long durationLong = Long.valueOf(duration);

        if (activity.equals("Sleeping") && durationLong >= twelveHoursLong) {
            System.out.println("WARNING !!! The patient slept for more than 12 hours !" );
            message = "WARNING !!! The patient slept for more than 12 hours !";
        }

        if (activity.equals("Leaving") && durationLong >= twelveHoursLong) {
            System.out.println("WARNING !!! The patient has been outside for more than 12 hours !");
            message = "WARNING !!! The patient has been outside for more than 12 hours !";
        }

        if ((activity.equals("Toileting") || activity.equals("Showering") || activity.equals("Grooming")) && durationLong >= oneHourLong) {
            System.out.println("WARNING !!! The patient spent more than one hour in the bathroom ! ");
            message = "WARNING !!! The patient spent more than one hour in the bathroom !";
        }

        return "";
    }

//    public String getMessage() {
//        return message;
//    }
}
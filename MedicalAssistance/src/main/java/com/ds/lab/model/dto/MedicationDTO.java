package com.ds.lab.model.dto;

import java.io.Serializable;

public class MedicationDTO implements Serializable {

    private long id;
    private String name;
    private String sideEffects;
    private String intakePerDay;

    public MedicationDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getIntakePerDay() {
        return intakePerDay;
    }

    public void setIntakePerDay(String intakePerDay) {
        this.intakePerDay = intakePerDay;
    }
}

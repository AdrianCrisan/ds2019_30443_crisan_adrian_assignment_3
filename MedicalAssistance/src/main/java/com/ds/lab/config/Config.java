package com.ds.lab.config;

import com.ds.lab.service.MedicationService;
import com.ds.lab.service.PatientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;

@Configuration
public class Config {

    private final PatientService patientService;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public Config(PatientService patientService) {
        this.patientService = patientService;
    }

    @Bean
    MedicationService medicationService() {
        return patientService;
    }

    @Bean
    RmiServiceExporter exporter(MedicationService medicationService) {
        Class<MedicationService> serviceInterface = MedicationService.class;
        RmiServiceExporter exporter = new RmiServiceExporter();
        exporter.setServiceInterface(serviceInterface);
        exporter.setService(medicationService);
        exporter.setServiceName(serviceInterface.getSimpleName());
        log.error(serviceInterface.getSimpleName());
        exporter.setRegistryPort(1099);
        return exporter;
    }

}
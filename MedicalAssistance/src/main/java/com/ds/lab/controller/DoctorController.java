package com.ds.lab.controller;

import com.ds.lab.model.Caretaker;
import com.ds.lab.model.Medication;
import com.ds.lab.model.MedicationPlan;
import com.ds.lab.model.Patient;
import com.ds.lab.model.dto.*;
import com.ds.lab.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class DoctorController {

    @Autowired
    private DoctorService doctorService;

    @GetMapping(value = "/doctor/patients/{id}")
    public Patient getPatientById(@PathVariable(value = "id") Long patientId) {
        return doctorService.getPatientById(patientId);
    }

    @GetMapping(value = "/doctor/caretakers/{id}")
    public Caretaker getCaretakerById(@PathVariable(value = "id") Long caretakerId) {
        return doctorService.getCaretakerById(caretakerId);
    }

    @GetMapping(value = "/doctor/patients")
    public List<PatientDTO> getPatients(@RequestParam("user") String username) {
        return doctorService.getAllPatients(username);
    }

    @GetMapping(value = "/doctor/caretakers")
    public List<CaretakerDTO> getCaretakers(@RequestParam("user") String username) {
        return doctorService.getAllCaretakers(username);
    }

    @GetMapping(value = "/doctor/medication")
    public List<MedicationDTO> getMedications() {
        return doctorService.getAllMedications();
    }

    @PostMapping(value = "/doctor/patients")
    public Patient createPatient(@RequestParam("doctor") String username, @Valid @RequestBody PatientIn patient) {
        return doctorService.createPatient(username, patient);
    }

    @PostMapping(value = "/doctor/caretakers")
    public Caretaker createCaretaker(@RequestParam("doctor") String username, @Valid @RequestBody CaretakerIn caretaker) {
        return doctorService.createCaretaker(username, caretaker);
    }

    @PostMapping(value = "/doctor/medication/plan")
    public MedicationPlan createMedicationPlan(@RequestParam("patientId") String patientId, @Valid @RequestBody MedicationPlanIn medicationPlan) {
        return doctorService.createMedicationPlan(patientId, medicationPlan);
    }

    @PostMapping(value = "doctor/medication")
    public Medication createMedication(@Valid @RequestBody MedicationIn medicationIn) {
        return doctorService.createMedication(medicationIn);
    }

    @PutMapping(value = "/doctor/patients/update/{id}")
    public Patient updatePatient(@PathVariable("id") Long patientId, @Valid @RequestBody Patient patient) {
        return doctorService.updatePatient(patientId, patient);
    }

    @PutMapping(value = "/doctor/caretakers/update/{id}")
    public Caretaker updateCaretaker(@PathVariable("id") Long caretakerId, @Valid @RequestBody Caretaker caretaker) {
        return doctorService.updateCaretaker(caretakerId, caretaker);
    }

    @DeleteMapping(value = "/doctor/patients/{id}")
    public Map<String, Boolean> deletePatient(@PathVariable(value = "id") Long patientId) {
        return doctorService.deletePatient(patientId);
    }

    @DeleteMapping(value = "/doctor/caretakers/{id}")
    public Map<String, Boolean> deleteCaretaker(@PathVariable(value = "id") Long caretakerId) {
        return doctorService.deleteCaretaker(caretakerId);
    }

    @DeleteMapping(value = "/doctor/medication/{id}")
    public Map<String, Boolean> deleteMedication(@PathVariable(value = "id") Long medicationId) {
        return doctorService.deleteMedication(medicationId);
    }
}

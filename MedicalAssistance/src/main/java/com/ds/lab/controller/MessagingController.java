package com.ds.lab.controller;

import com.ds.lab.service.ConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

@Controller
@CrossOrigin(origins = "*")
public class MessagingController {

    @Autowired
    ConsumerService consumerService;

    @MessageMapping("/message")
    @SendTo("/api/topic")
    public String send(String message) throws Exception {
//        while (true) {
//            Thread.sleep(1000);
//            return consumerService.getMessage();
//        }

        return "TEST";
    }
}
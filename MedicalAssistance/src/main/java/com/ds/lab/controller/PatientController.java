package com.ds.lab.controller;

import com.ds.lab.model.dto.PatientDTO;
import com.ds.lab.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api")
public class PatientController {

    @Autowired
    private PatientService patientService;

    @GetMapping(value = "/patient")
    public PatientDTO getPatientInfo(@RequestParam("user") String username) {
        return patientService.getPatientInfo(username);
    }
}

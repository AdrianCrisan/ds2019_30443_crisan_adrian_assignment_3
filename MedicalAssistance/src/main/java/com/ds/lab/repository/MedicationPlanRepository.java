package com.ds.lab.repository;

import com.ds.lab.model.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Long> {

    @Query(value = "SELECT * FROM medication_plan WHERE medication_plan.patient_id=?1", nativeQuery = true)
    List<MedicationPlan> findMedicationPlansOfPatient(Long patient_id);
}

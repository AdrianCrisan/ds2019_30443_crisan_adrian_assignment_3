package com.ds.lab.repository;

import com.ds.lab.model.Caretaker;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CaretakerRepository extends UserRepository<Caretaker> {

    Caretaker findByUsername(String username);

    void deleteByUsername(String username);

    @Query(value = "SELECT * FROM Caretaker c WHERE doctor_id=?1", nativeQuery = true)
    List<Caretaker> findAllCaretakersOfDoctor(Long doctor_id);
}

import {Component, OnInit} from '@angular/core';
import {MedicationService} from './medication.service';
import {MedicationPlan} from './medication-plan';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  medPlans: MedicationPlan[] = [];

  constructor(private medicationService: MedicationService) {}

  ngOnInit() {
    this.medicationService.getMedicationPlans('patient1').subscribe(
      data => {
        this.medPlans = data;
        console.log(data);
      }
    );
  }

  setTaken(medId: string) {
    this.medicationService.sendNotification(medId, 'patient1').subscribe(
      data => console.log(data)
    );
  }
}

import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';

@Injectable({
  providedIn: 'root'
})
export class MedicationService {

  private medicationUrl = 'http://localhost:8083/api/medication';

  constructor(private http: HttpClient) { }

  getMedicationPlans(username: string): Observable<any> {
    return this.http.get(this.medicationUrl, {params: {user: username}});
  }

  sendNotification(id: string, username: string): Observable<any> {
    return this.http.put(this.medicationUrl, {}, {params: {medId: id, patient: username}});
  }
}

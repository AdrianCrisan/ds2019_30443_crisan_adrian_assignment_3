import {Medication} from './medication';

export class MedicationPlan {

  treatmentPeriod: number;
  medications: string[];
  receivedMedications?: Medication[];
}
